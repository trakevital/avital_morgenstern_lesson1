#pragma once
#ifndef QUEUE_H
#define QUEUE_H

#include <cstdlib> 

#define EMPTY_QUEUE -1


/* a queue contains positive integer values. */
typedef struct queue
{
	int* numbers;
	int maxAmount;
	int count;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */