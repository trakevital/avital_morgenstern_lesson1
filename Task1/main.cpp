#include "queue.h"
#include <iostream>
#define STD_QUEUE_LEN 4048


int main()
{
	queue* q = new(queue);
	int i = 0;
	initQueue(q, STD_QUEUE_LEN);
	for (i = 0; i < STD_QUEUE_LEN; i++)
	{
		enqueue(q, i);
	}
	while (q->count)
	{
		std::cout << dequeue(q) << std::endl;
	}
	cleanQueue(q);
	return 0;
}