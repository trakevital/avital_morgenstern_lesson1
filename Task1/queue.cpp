#include "queue.h"


/*This function creates a queue with the potential length given in the parameter size
Input: a queue pointer, the potential size of the queue
Ouput: None
*/
void initQueue(queue* q, unsigned int size)
{
	q->count = 0;
	q->maxAmount = size;
	q->numbers  = (int*)std::calloc(size, sizeof(int));
}


/*This function deletes a queue structure completely
Input: a queue pointer
Output: None
*/
void cleanQueue(queue* q)
{
	while (EMPTY_QUEUE != dequeue(q))
	{
	}

	delete(q);
}

/*This function addes an uint to the back of the queue
Input: a queue struct and a value to append
Output: None
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->count < q->maxAmount)
	{
		q->numbers[q->count] = newValue;
		q->count++;
	}
}


/*This function removes the first element in the queue
Input: the queue
Ouput: the first element, if the queue is empty returns EMPTY_QUEUE (-1)
*/
int dequeue(queue* q)// return element in top of queue, or -1 if empty
{
	int firstNumber = EMPTY_QUEUE;
	int i = 0;
	int* newNumbers = (int*)calloc(q->maxAmount, sizeof(int));

	if (q->count && newNumbers && q->numbers)
	{
		firstNumber = q->numbers[0];
		q->count--;
		for (i = 1; i < q->maxAmount; i++)
		{
			newNumbers[i - 1] = q->numbers[i];
		}
		q->numbers = (int*)std::realloc(newNumbers, q->maxAmount * sizeof(int));
	}
	return firstNumber;
}