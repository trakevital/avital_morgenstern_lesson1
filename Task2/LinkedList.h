#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "stack.h"
#define EMPTY_ITEM -1
void addToStack(stack** head, unsigned int num);

int removeFromStack(stack** head);

#endif // LINKED_LIST_H