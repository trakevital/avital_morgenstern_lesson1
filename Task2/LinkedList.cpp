#include "LinkedList.h"

void addToStack(stack** head, unsigned int num)
{
	stack* curr = (*head)->_next;
	(*head)->_next = new stack;
	(*head)->_next->_next = curr;
	(*head)->_next->_num = num;
}

int removeFromStack(stack** head)
{
	stack* curr = (*head)->_next ? (*head)->_next->_next : nullptr;
	int num = (*head)->_next ? (*head)->_next->_num : EMPTY_ITEM;
	delete((*head)->_next);
	(*head)->_next = curr;
	return num;
}