#include "stack.h"
#include "LinkedList.h"

void push(stack* s, unsigned int element)
{
	addToStack(&s, element);
}



int pop(stack* s)
{
	return removeFromStack(&s);
}

void initStack(stack* s)
{
	
	s->_next = nullptr;
}

void cleanStack(stack* s)
{
	stack* next = nullptr;
	while (s->_next)
	{
		next = s->_next;
		delete(s);
		s = next;
	}
	delete(s);
}